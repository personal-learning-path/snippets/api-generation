# API Generation from YAML
> Spring Boot version: 2.3.4.RELEASE

Example project in Spring Boot for demonstrating how to generate a REST API using the `swagger-codegen-maven-plugin`
plugin.

## Background
Creating a REST API from the ground up takes always some time to properly configure and use the correct validations
on the endpoints. If you want to go a step further and document your API using Swagger then you will need to invest
again time to add the correct annotations on the methods, fields and classes. But this should not be a pain and we can
achieve both of them using a simple generation mechanism provided by the `swagger-codegen-maven-plugin`.

The `swagger-codegen-maven-plugin` is a plugin for Maven and the main purpose of it is to read `YAML` files,
parse the content and generate the Java classes and methods based on it. The `YAML` files allows you to configure
your APIs using the [OpenAPI Specification v2](https://swagger.io/specification/v2/) syntax. You can check the plugin's [documentation](https://github.com/swagger-api/swagger-codegen/tree/master/modules/swagger-codegen-maven-plugin)
for more details.

The first step is to create your `YAML` file in which you define the API:  
**animals-api.yml**
```yaml
swagger: "2.0"
info:
  description: "Allows different operations on the Animal component"
  version: "1.0.0"
  title: "Animals"
  termsOfService: "http://swagger.io/terms/"
  contact:
    email: "georgeberar.contact@gmail.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"


# Base path and http scheme
host: "swagger.io"
basePath: /api
schemes:
  - http
  - https


# Security Definition
#securityDefinitions:
#  basicAuth:
#    type: basic


# Apply the Security Definition to entire API
#security:
#  - basicAuth: []


# Endpoints
paths:
  ## Get animal by id
  /animals/{id}:
    get:
      operationId: getAnimalById
      description: Get animal by id
      summary: Get animal by id
      produces:
        - application/json
      parameters:
        - in: path
          name: id
          description: the animal's id
          type: string
          required: true
          minLength: 36
          maxLength: 36
          example: '123e4567-e89b-12d3-a456-426655440000'
      responses:
        200:
          description: Response contains the animal details.
          schema:
            $ref: "./dto/animals-dto.yml#/definitions/AnimalDetailsDTO"
        400:
          description: Bad Request.
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: The animal with specified id was not found.
        500:
          description: An internal server error occured.

```

Notice that for the `200` response case we used the `$ref` syntax for specifying that the DTO is defined
in another file:

**animals-dto.yml**
```yaml
# DTO definitions
definitions:
  AnimalDetailsDTO:
    description: Defines the structure of the response about the animal details
    type: object
    properties:
      id:
        description: The id of the animal
        type: string
        example: '123e4567-e89b-12d3-a456-426655440000'
      name:
        description: The name of the animal
        type: string
        example: 'Lion'
```

After you defined the endpoints and DTOs you need to use the Maven plugin to configure an execution for your
files:  
**pom.xml**
```xml
            <!-- Swagger codegen plugin -->
            <plugin>
                <groupId>io.swagger</groupId>
                <artifactId>swagger-codegen-maven-plugin</artifactId>
                <version>${swagger-codegen-maven-plugin.version}</version>
                <executions>
                    <!-- Generate Animals API -->
                    <execution>
                        <id>generate-animals-api</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${project.basedir}/src/main/resources/apis/animals/animals-api.yml</inputSpec>
                            <language>spring</language>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <modelPackage>com.learningpath.snippets.apigeneration.animals.dto</modelPackage>
                            <apiPackage>com.learningpath.snippets.apigeneration.animals.api</apiPackage>
                            <configOptions>
                                <sourceFolder>src/gen/java/main</sourceFolder>
                                <interfaceOnly>true</interfaceOnly>
                                <recursiveBeanValidation>true</recursiveBeanValidation>
                                <useBeanValidation>true</useBeanValidation>
                                <serializableModel>true</serializableModel>
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
```

The last step is to generate the API using the Maven's `generate-sources` command which will trigger the plugin.

## Test case
For demonstrating the API generation we have defined a simple API for fetching details of an animal based on its id.
We want to be able to use the API from [Postman](https://www.postman.com/) but also from Swagger UI.

## Demo
1. Postman  
![Get_animal_by_id_Postman](/uploads/c2d799fc8086b8c7778c579f96b44b26/Get_animal_by_id_Postman.PNG)

2. Swagger UI
The console is accessible under `http://localhost:8080/swagger-ui.html`.  
![Get_animal_by_id_Swagger](/uploads/758494d3ec211121e21c743f2d55cae7/Get_animal_by_id_Swagger.PNG)

## Snippet
The snippet created for this project can be found [here](https://gitlab.com/-/snippets/2033037).

## Contact
You can contact me at **georgeberar.contact@gmail.com**.
