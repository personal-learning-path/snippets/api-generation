package com.learningpath.snippets.apigeneration.configuration;


import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static com.google.common.base.Predicates.or;

/**
 * Class used to configure the Swagger UI dashboard.
 * <p>
 * Defines the exposed endpoints structured under semantic groups for each personal REST API,
 * additional metadata and the security configurations to allow only secured calls.
 *
 * @author George
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    /**
     * Configures the docket for scanning and exposing the endpoints of the Animals API.
     *
     * @return {@link Docket} containing the definitions
     */
    @Bean
    public Docket createUsersApi() {
        final Predicate<String> endpointsPaths = or(PathSelectors.regex("/animals.*"));
        return createApiDefinition("ANIMALS", endpointsPaths, "Animals API");
    }


    /**
     * Creates the definition for an API.
     *
     * @return a {@link Docket} containing the definition of this API
     */
    private Docket createApiDefinition(final String groupName, final Predicate paths, final String title) {
        //@formatter:off
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.learningpath.snippets.apigeneration"))
                .paths(paths)
                .build()
                .apiInfo(createMetaInfo(title))
                .useDefaultResponseMessages(false);
        //@formatter:on
    }


    /**
     * Creates the metadata for an API.
     *
     * @return a {@link ApiInfo} containing the custom metadata of this API
     */
    private ApiInfo createMetaInfo(final String title) {
        //@formatter:off
        return new ApiInfo(
                title,
                "Defines the endpoints exposed by the REST API",
                "",
                "http://swagger.io/terms/",
                new Contact("George Berar", "", "georgeberar.contact@gmail.com"),
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0.html",
                Collections.emptyList()
        );
        //@formatter:on
    }

}
