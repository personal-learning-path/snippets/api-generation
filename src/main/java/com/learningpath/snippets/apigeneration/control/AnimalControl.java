package com.learningpath.snippets.apigeneration.control;

import com.learningpath.snippets.apigeneration.animals.dto.AnimalDetailsDTO;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Control class for providing business behavior on animal resource.
 *
 * @author George
 */
@Service
public class AnimalControl {

    private static final List<AnimalDetailsDTO> animals;

    static {
        final AnimalDetailsDTO lion = new AnimalDetailsDTO();
        lion.setId("123e4567-e89b-12d3-a456-426655440000");
        lion.setName("LION");

        final AnimalDetailsDTO snake = new AnimalDetailsDTO();
        snake.setId("123e4567-e89b-12d3-a456-426655440001");
        snake.setName("SNAKE");

        animals = Arrays.asList(lion, snake);
    }

    /**
     * Finds an animal by id.
     *
     * @param id - the id of the animal
     * @return an {@code Optional} with the animal details inside if found or empty otherwise
     */
    public Optional<AnimalDetailsDTO> findAnimalById(final String id) {
        return animals.stream().filter(animal -> animal.getId().equals(id)).findFirst();
    }

}
