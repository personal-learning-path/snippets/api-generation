package com.learningpath.snippets.apigeneration.boundary;

import com.learningpath.snippets.apigeneration.animals.api.AnimalsApi;
import com.learningpath.snippets.apigeneration.animals.dto.AnimalDetailsDTO;
import com.learningpath.snippets.apigeneration.control.AnimalControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Boundary for exposing endpoints of Animals API.
 *
 * @author George
 */
@RestController
@Validated
public class AnimalsBoundary implements AnimalsApi {

    @Autowired
    private AnimalControl animalControl;


    /**
     * Get animal by id.
     *
     * @param id - the id of the animal
     * @return the animal details if found or 404 otherwise
     */
    @Override
    public ResponseEntity<AnimalDetailsDTO> getAnimalById(@Size(min = 36, max = 36) String id) {
        // find animal by id
        final Optional<AnimalDetailsDTO> existingAnimal = this.animalControl.findAnimalById(id);
        // return response
        return existingAnimal.map(animalDetailsDTO -> new ResponseEntity<>(animalDetailsDTO, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
